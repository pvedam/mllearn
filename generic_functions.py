def get_contract_year_annual_contract(contract_period, date):
    # Here contract period is not annual
    date_split = date.split('-')
    date_year = date_split[0]
    date_month = date_split[1]
    date_day = date_split[2]
    date_month_day = date_month + '-' + date_day
    if (contract_period['start'] == '01-01' and contract_period['end'] == '12-31') or (contract_period['start'] <= date_month_day <= '12-31'):
        contract_year = int(date_year)
    elif '01-01' <= date_month_day <= contract_period['end']:
        contract_year = int(date_year) - 1

    return contract_year