import pandas as pd
import psycopg2
import numpy as np
import math

conn = psycopg2.connect(dbname="mllearn", user="postgres", password="aditya12#", host="localhost")
cur = conn.cursor()


def load_rvu_file(file_path, year:int):
    df = pd.read_excel(file_path, skiprows=9, skipfooter=1)
    df = df[["HCPCS", "MOD", "DESCRIPTION", "RVU", "PE RVU", "PE RVU.1", "RVU.1", "FACTOR"]]
    df['year'] = year
    df.replace(np.nan, '', regex=True, inplace=True)
    df.rename(index=str, columns={
        'HCPCS': 'HCPCS'.lower(),
        'MOD': 'MOD'.lower(),
        'DESCRIPTION': 'DESCRIPTION'.lower(),
        'RVU': 'work_rvu',
        'PE RVU': 'non_fac_pe_rvu',
        'PE RVU.1': 'fac_pe_rvu',
        'RVU.1': 'mp_rvu',
        'FACTOR': 'cf'
    },
                inplace=True)
    # print(data[["HCPCS", "MOD", "DESCRIPTION", "RVU", "PE RVU", "PE RVU.1", "RVU.1"]])
    # print(data.columns.values)
    # Now have to store the value in db
    # Push all these value in
    # Create a temp table and push all the data in that table
    # update entries which are already there with these new entries
    # insert the missing entries from here to there
    # drop this temp table

    # drop_table = "drop table if exists public.temp_rvu_data ;"
    # cur.execute(drop_table)
    query = """
        drop table if exists public.temp_rvu_data ;
        select * into public.temp_rvu_data from public.rvu_data where 1=2;
    """
    cur.execute(query)
    conn.commit()

    # Now insert records in this table
    for index, row in df.iterrows():
        cur.execute("INSERT INTO public.temp_rvu_data (hcpcs,mod,description,work_rvu,non_fac_pe_rvu,fac_pe_rvu,mp_rvu,cf,year) "
                    "values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    (row['hcpcs'],
                    row['mod'],
                    row['description'],
                    row['work_rvu'],
                    row['non_fac_pe_rvu'],
                    row['fac_pe_rvu'],
                    row['mp_rvu'],
                    row['cf'],
                    row['year'])
                    )

    query = """
    update public.rvu_data a
        set description=b.description,
        work_rvu=b.work_rvu,
        fac_pe_rvu=b.fac_pe_rvu,
        mp_rvu=b.mp_rvu,
        cf=b.cf
    from public.temp_rvu_data b 
    where a.hcpcs=b.hcpcs
    and a.mod=b.mod
    and a.year=b.year
    """

    cur.execute(query)

    query = """
    insert into public.rvu_data
    (hcpcs,mod,description,work_rvu,non_fac_pe_rvu,fac_pe_rvu,mp_rvu,cf,year)
    select distinct a.hcpcs,a.mod,a.description,a.work_rvu,a.non_fac_pe_rvu,a.fac_pe_rvu,a.mp_rvu,a.cf,a.year
    from public.temp_rvu_data a 
    left join public.rvu_data b 
     on a.hcpcs=b.hcpcs
     and a.mod=b.mod
     and a.year=b.year
    where b.hcpcs is null
    """

    cur.execute(query)

    conn.commit()

    # drop temp table now

    cur.execute("drop table if exists public.temp_rvu_data")
    conn.commit()


def load_gpci_file(file_path, year:int):
    df = pd.read_excel(file_path, skiprows=2, skipfooter=9)
    df['year'] = year
    for column_name in df.columns.values:
        df.rename(index=str, columns={column_name: column_name.strip()}, inplace=True)

    df.rename(index=str, columns={
        'Unnamed: 0': 'medicare_administrative_contractor',
        'Unnamed: 1': 'locality_number',
        'Unnamed: 2': 'locality_name',
        'PW GPCI': 'work_gpci',
        'PE GPCI': 'pe_gpci',
        'MP GPCI': 'mp_gpci'
    },
                inplace=True)

    df.replace(np.nan, '', regex=True, inplace=True)

    df['medicare_administrative_contractor'] = df['medicare_administrative_contractor'].map(lambda x: str(x).zfill(5))
    df['locality_number'] = df['locality_number'].map(lambda x: str(x).zfill(2))
    # for column_name in data.columns.values:
    #     data.rename(index=str, columns={column_name: '_'.join(column_name.lower().strip().split())}, inplace=True)

    query = """
            drop table if exists public.temp_gpci_data ;
            select * into public.temp_gpci_data from public.gpci_data where 1=2;
        """
    cur.execute(query)
    conn.commit()

    # Now insert records in this table
    for index, row in df.iterrows():
        cur.execute(
            "INSERT INTO public.temp_gpci_data "
            "(medicare_administrative_contractor,locality_number,locality_name,work_gpci,pe_gpci,mp_gpci,year) "
            "values (%s, %s, %s, %s, %s, %s, %s)",
            (row['medicare_administrative_contractor'],
             row['locality_number'],
             row['locality_name'],
             row['work_gpci'],
             row['pe_gpci'],
             row['mp_gpci'],
             row['year'])
            )

    query = """
        update public.gpci_data a
            set locality_name=b.locality_name,
            work_gpci=b.work_gpci,
            pe_gpci=b.pe_gpci,
            mp_gpci=b.mp_gpci
        from public.temp_gpci_data b 
        where a.medicare_administrative_contractor=b.medicare_administrative_contractor
        and a.locality_number=b.locality_number
        and a.year=b.year
        """

    cur.execute(query)

    query = """
        insert into public.gpci_data
        (medicare_administrative_contractor,locality_number,locality_name,work_gpci,pe_gpci,mp_gpci,year)
        select distinct a.medicare_administrative_contractor,a.locality_number,a.locality_name,a.work_gpci,a.pe_gpci,a.mp_gpci,a.year
        from public.temp_gpci_data a 
        left join public.gpci_data b 
         on a.medicare_administrative_contractor=b.medicare_administrative_contractor
         and a.locality_number=b.locality_number
         and a.year=b.year
        where b.medicare_administrative_contractor is null
        """

    cur.execute(query)

    conn.commit()

    # drop temp table now

    cur.execute("drop table if exists public.temp_gpci_data")
    conn.commit()


def load_zip_file(file_path):
    df = pd.read_excel(file_path, skipfooter=1)
    df['ZIP CODE'] = df['ZIP CODE'].map(lambda x: str(x).zfill(5))
    df['CARRIER'] = df['CARRIER'].map(lambda x: str(x).zfill(5))
    df['LOCALITY'] = df['LOCALITY'].map(lambda x: str(x).zfill(2))
    df['year'] = df['YEAR/QTR'].map(lambda x: str(int(x))[:4])
    df['quarter'] = df['YEAR/QTR'].map(lambda x: str(int(x))[4])

    df = df[['STATE', 'ZIP CODE', 'CARRIER', 'LOCALITY', 'year', 'quarter']]

    for column_name in df.columns.values:
        df.rename(index=str, columns={column_name: '_'.join(column_name.strip().lower().split())}, inplace=True)

    query = """
                drop table if exists public.temp_zip_carrier_locality ;
                select * into public.temp_zip_carrier_locality from public.zip_carrier_locality where 1=2;
            """
    cur.execute(query)
    conn.commit()

    # Now insert records in this table
    for index, row in df.iterrows():
        cur.execute(
            "INSERT INTO public.temp_zip_carrier_locality "
            "(state,zip_code,carrier,locality,year,quarter) "
            "values (%s, %s, %s, %s, %s, %s)",
            (row['state'],
             row['zip_code'],
             row['carrier'],
             row['locality'],
             row['year'],
             row['quarter'])
        )

    query = """
            update public.zip_carrier_locality a
                set carrier=b.carrier,
                locality=b.locality
            from public.temp_zip_carrier_locality b
            where a.state=b.state
            and a.zip_code=b.zip_code
            and a.year=b.year
            and a.quarter=b.quarter
            """

    cur.execute(query)

    query = """
            insert into public.zip_carrier_locality
            (state,zip_code,carrier,locality,year,quarter)
            select distinct a.state,a.zip_code,a.carrier,a.locality,a.year,a.quarter
            from public.temp_zip_carrier_locality a
            left join public.zip_carrier_locality b
             on a.state=b.state
             and a.zip_code=b.zip_code
             and a.year=b.year
             and a.quarter=b.quarter
            where b.state is null
            """

    cur.execute(query)

    conn.commit()

    # drop temp table now

    cur.execute("drop table if exists public.temp_zip_carrier_locality")
    conn.commit()


def find_price(hcpcs_code, modifier, zip_code, in_facility, date_str):
    # Looking at the date find the year and the quarter
    # assuming date to be in the format YYYY-MM-DD
    date_str_splitted = date_str.strip().split('-')
    # print(date_str_splitted)
    year = int(date_str_splitted[0])
    # lets find quarter
    quarter = math.ceil(int(date_str_splitted[1]) / 3)

    cur.execute("""
        select distinct 
            rv.non_fac_pe_rvu,
            rv.fac_pe_rvu,
            rv.mp_rvu,
            rv.work_rvu,
            rv.cf,
            gd.work_gpci,
            gd.pe_gpci,
            gd.mp_gpci
        from public.zip_carrier_locality zcl 
            inner join public.gpci_data gd
                on zcl.zip_code=%s
                and zcl.carrier=gd.medicare_administrative_contractor
                and zcl.locality=gd.locality_number
                and zcl.year=%s
                and zcl.quarter=%s
                and zcl.year=gd.year
            inner join public.rvu_data rv
                on rv.year=gd.year
                and rv.hcpcs=%s
                and rv.mod=%s
    """, (zip_code, year, quarter, hcpcs_code, modifier))

    result_temp = cur.fetchall()
    if result_temp:
        result = result_temp[0]
        # Let's calculate the price
        non_fac_pe_rvu, fac_pe_rvu, mp_rvu, work_rvu, cf, work_gpci, pe_gpci, mp_gpci = result
        if in_facility:
            pe_rvu = fac_pe_rvu
        else:
            pe_rvu = non_fac_pe_rvu

        price = (pe_rvu * pe_gpci +  work_rvu * work_gpci + mp_rvu * mp_gpci) * cf
        return price
    else:
        print('Entry Not Found')


# load_gpci_file('C:\\Machine_Learning\\RVU18B\\GPCI2018.xlsx', 2018 )
# load_rvu_file('C:\\Machine_Learning\\RVU18B\\PPRRVU18_APR.xlsx', 2018)
# load_zip_file('C:\\Machine_Learning\\Zip-Code-to-Carrier-Locality\\ZIP5_OCT2018.xlsx')

print(find_price('91022', '26', '95164', True, '2018-10-06'))

