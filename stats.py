import pandas as pd
import numpy as np
import tempfile
import os
from datetime import datetime
import statistics
from scipy import stats
import json
import psycopg2
import copy
from calendar import monthrange
from functools import reduce
from generic_functions import get_contract_year_annual_contract

is_innetwork_when_in_network = True

conn = psycopg2.connect(dbname="mllearn", user="postgres", password="aditya12#", host="localhost")
cur = conn.cursor()

def do_basic_load(deidentified_claim_file_path, betos_file_path, betos_procedure_mapping_path):
    claims = pd.read_csv(deidentified_claim_file_path,
                         sep='\t',
                         low_memory=False,
                         parse_dates=['dob', 'service_start_date', 'service_end_date', 'paid_date']
                         )

    claims['provider_city_state'] = claims['provider_city'] + ', ' + claims['provider_state']
    claims['service_year'] = claims['service_start_date'].map(lambda x: x.year)

    if 'cob_amount' not in claims:
        claims['cob_amount'] = 0.0

    betos_names = pd.read_csv(betos_file_path,
                              sep='\t',
                              low_memory=False
                              )

    betos = pd.read_csv(betos_procedure_mapping_path,
                        sep=' ',
                        low_memory=False
                        )

    betos.drop(columns=['date'], inplace=True)
    betos['betos'] = betos['betos'].apply(lambda x: x[:3])

    # Finding the betos corresponding to ov/lab/imaging
    betos['betos_type'] = ''

    # Setting betos types
    betos.loc[betos.betos.str.startswith('M1')
              | betos.betos.str.startswith('M5')
              | betos.betos.str.startswith('M6'), 'betos_type'] = 'ov'

    betos.loc[betos.betos.str.startswith('T1'), 'betos_type'] = 'lab'
    betos.loc[betos.betos.str.startswith('I'), 'betos_type'] = 'imaging'

    ov_betos = betos[betos['betos_type'] == 'ov']
    lab_betos = betos[betos['betos_type'] == 'lab']
    imaging_betos = betos[betos['betos_type'] == 'imaging']

    betos_concated = pd.concat([ov_betos, lab_betos, imaging_betos], axis=0)

    return claims, betos_names, betos_concated


def get_stats(deidentified_claim_file_path, betos_file_path, betos_procedure_mapping_path):

    name_of_output_xlsx = os.path.join(tempfile.gettempdir(), 'stats_claims_{}.xlsx'.format(
        datetime.now().strftime('%Y%m%d%H%M%S')))
    # at the moment is_innetwork is reverse, i.e. is_innetwork = False means its in network
    # is_innetwork_when_in_network = True

    claims, betos_names, betos_concated = do_basic_load(deidentified_claim_file_path, betos_file_path, betos_procedure_mapping_path)

    total_claim_count = claims.shape[0]
    oon_claims = claims[claims['is_innetwork'] != is_innetwork_when_in_network]
    inn_claims = claims[claims['is_innetwork'] == is_innetwork_when_in_network]
    inn_with_empty_npi_claims = inn_claims[inn_claims['provider_npi'] == 0]

    inn_with_non_empty_cob_claim = inn_claims[inn_claims['cob_amount'] != 0]
    inn_with_empty_allowed_amount_claim = inn_claims[inn_claims['allowed_amount'] == 0]
    inn_with_non_empty_modifiers = inn_claims[inn_claims['modifier'].notnull()]

    # Useful claims mean INN with non zero NPI, zero COB amount and null modifier

    useful_inn_claims = inn_claims[(inn_claims['provider_npi'] != 0)
                                   & (inn_claims['cob_amount'] == 0)
                                   & (inn_claims['modifier'].isnull())
                                   & (inn_claims['allowed_amount'] != 0)]

    oon_claims_count = oon_claims.shape[0]
    inn_claims_count = inn_claims.shape[0]
    inn_with_empty_npi_claims_count = inn_with_empty_npi_claims.shape[0]
    inn_with_non_empty_cob_claims_count = inn_with_non_empty_cob_claim.shape[0]
    inn_with_empty_allowed_amount_claim_count = inn_with_empty_allowed_amount_claim.shape[0]
    inn_with_non_empty_modifiers_count = inn_with_non_empty_modifiers.shape[0]
    useful_inn_claims_count = useful_inn_claims.shape[0]
    useful_inn_claims_unique_provider_count = useful_inn_claims['provider_npi'].nunique()
    useful_inn_claims_unique_procedure_count = useful_inn_claims['procedure'].nunique()

    # We don't know types of providers at the moment, so we can't find unique number of practitioners and facilities
    # print(total_claim_count)
    # print(oon_claims_count)
    # print(inn_claims_count)
    # print(inn_with_empty_npi_claims_count)
    # print(inn_with_non_empty_cob_claims_count)
    # print(useful_inn_claims_count)
    # print(useful_inn_claims_unique_provider_count)
    # print(useful_inn_claims_unique_procedure_count)

    # Lets prepare one data frame with above data which can be then written to excel

    df = pd.DataFrame({
        'Name': ['total_claims_count',
                 'oon_claims_count',
                 'inn_claims_count',
                 'inn_with_empty_npi_claims_count',
                 'inn_with_non_empty_cob_claims_count',
                 'inn_with_empty_allowed_amount_claim_count',
                 'inn_with_non_empty_modifiers_count',
                 'useful_inn_claims_count',
                 'useful_inn_claims_unique_provider_count',
                 'useful_inn_claims_unique_procedure_count'
                 ],
        'Result': [
            total_claim_count,
            oon_claims_count,
            inn_claims_count,
            inn_with_empty_npi_claims_count,
            inn_with_non_empty_cob_claims_count,
            inn_with_empty_allowed_amount_claim_count,
            inn_with_non_empty_modifiers_count,
            useful_inn_claims_count,
            useful_inn_claims_unique_provider_count,
            useful_inn_claims_unique_procedure_count
        ]
    })

    writer = pd.ExcelWriter(name_of_output_xlsx, engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Basic Stats', index=False)
    worksheet = writer.sheets['Basic Stats']
    offset = len(df) + 2


    useful_inn_claims_per_betos_type_stats = useful_inn_claims\
        .merge(betos_concated, how="inner", on=['procedure'])\
        .groupby(['betos_type'])\
        .agg({'procedure': 'nunique', 'provider_npi': 'nunique', 'claim_number': 'count'})\
        .reset_index()\
        .rename(columns={'betos_type': 'procedure_type',
                         'procedure': 'unique_procedure_count',
                         'provider_npi': 'unique_provider_count',
                         'claim_number': 'claim_count'}
                )

    worksheet.write(offset, 0, 'Stats for Useful INN Claims')
    offset += 1
    useful_inn_claims_per_betos_type_stats.to_excel(writer, sheet_name='Basic Stats', index=False, startrow=offset)

    betos_concated.to_excel(writer, sheet_name='Betos List', index=False)
    # print(useful_inn_claims_per_betos_type_stats)

    # Finding top 3 cities
    useful_claims_top_3_cities = useful_inn_claims.groupby(['provider_city_state'])['claim_number']\
        .count().reset_index(name='claim_count').sort_values(['claim_count'], ascending=False)[['provider_city_state']].head(3)

    # print(useful_claims_top_3_cities)

    useful_cities_claims = useful_claims_top_3_cities.merge(claims, how="inner", on=['provider_city_state'])

    useful_cities_claim_inn_onn_count_stats = useful_cities_claims.groupby(['provider_city_state']) \
        .agg({'is_innetwork': [('claim_count', 'count'),
                               ('inn_claim_count', (
                                   lambda x: sum([1 if item == is_innetwork_when_in_network else 0 for item in x]))
                                ),
                               ('oon_claim_count', (
                                   lambda x: sum([0 if item == is_innetwork_when_in_network else 1 for item in x]))
                                )
                               ]
              })

    useful_cities_claim_inn_onn_count_stats.columns = useful_cities_claim_inn_onn_count_stats.columns.droplevel(0)
    useful_cities_claim_inn_onn_count_stats.to_excel(writer, sheet_name='Stats for top 3 Cities')
    worksheet = writer.sheets['Stats for top 3 Cities']
    offset = len(useful_cities_claim_inn_onn_count_stats) + 2

    # print(useful_cities_claim_inn_onn_count_stats)

    useful_cities_claim_inn_no_npi_non_zero_cob = useful_cities_claims[useful_cities_claims['is_innetwork'] == is_innetwork_when_in_network]\
        .groupby(['provider_city_state']).agg(
        {
            'provider_npi': (lambda x: sum(x == 0)),
            'cob_amount': lambda x: sum(x != 0),
            'modifier': lambda x: sum([0 if pd.isna(item) else 1 for item in x]),
            'allowed_amount': lambda x: sum(x == 0)
        }).reset_index()\
        .rename(columns={'provider_npi': 'claim_count_no_npi',
                         'cob_amount': 'claim_count_nonzero_cob',
                         'modifier': 'claim_count_non_empty_modifier',
                         'allowed_amount': 'claim_count_zero_allowed_amount'
                         })

    worksheet.write(offset, 0, 'Stats for INN claims')
    offset += 1
    useful_cities_claim_inn_no_npi_non_zero_cob.to_excel(writer, sheet_name='Stats for top 3 Cities', index=False, startrow=offset)
    offset += len(useful_cities_claim_inn_no_npi_non_zero_cob) + 2

    useful_cities_useful_inn_claims = useful_cities_claims[(useful_cities_claims['is_innetwork'] == is_innetwork_when_in_network)
                                                           & (useful_cities_claims['provider_npi'] != 0)
                                                           & (useful_cities_claims['cob_amount'] == 0)
                                                           & (useful_cities_claims['modifier'].isnull())
                                                           & (useful_cities_claims['allowed_amount'] != 0)]

    useful_cities_useful_inn_claims_count_stats = useful_cities_useful_inn_claims.groupby(['provider_city_state']).agg({
        'claim_number': 'count',
        'provider_npi': 'nunique',
        'procedure': 'nunique'
    }).reset_index().rename(columns={'claim_number': 'claim_count',
                                     'provider_npi': 'unique_provider_count',
                                     'procedure': 'unique_procedure_count'})

    worksheet.write(offset, 0, 'Useful INN claims stats')
    offset += 1
    useful_cities_useful_inn_claims_count_stats.to_excel(writer, sheet_name='Stats for top 3 Cities', index=False, startrow=offset)
    offset += len(useful_cities_useful_inn_claims_count_stats) + 2

    # print(useful_cities_useful_inn_claims_count_stats)

    useful_cities_useful_inn_claims_per_betos_type_stats = useful_cities_useful_inn_claims \
        .merge(betos_concated, how="inner", on=['procedure']) \
        .groupby(['provider_city_state', 'betos_type']) \
        .agg({'procedure': 'nunique', 'provider_npi': 'nunique', 'claim_number': 'count'}) \
        .reset_index() \
        .rename(columns={'betos_type': 'procedure_type',
                         'procedure': 'unique_procedure_count',
                         'provider_npi': 'unique_provider_count',
                         'claim_number': 'claim_count'})

    worksheet.write(offset, 0, 'Useful INN claims stats')
    offset += 1
    useful_cities_useful_inn_claims_per_betos_type_stats.to_excel(writer, sheet_name='Stats for top 3 Cities', index=False, startrow=offset)
    offset += len(useful_cities_useful_inn_claims_per_betos_type_stats) + 2

    # print(useful_cities_useful_inn_claims_per_betos_type_stats)

    useful_cities_userful_inn_claims_with_betos = useful_cities_useful_inn_claims.merge(betos_concated, how="inner", on=['procedure'])

    min_service_year = useful_cities_userful_inn_claims_with_betos['service_year'].min()
    max_service_year = useful_cities_userful_inn_claims_with_betos['service_year'].max()

    pivot_table = pd.pivot_table(useful_cities_userful_inn_claims_with_betos,
                                 index=['provider_city_state', 'betos_type', 'provider_npi', 'procedure'],
                                 columns=['service_year'],
                                 aggfunc={
                                     'claim_number': [('claim_count', 'count')],
                                     'allowed_amount': [('allowed_amounts', lambda x: ','.join(str(k/100) for k in set(x))),
                                                        ('is_amount_same', lambda x: "Single" if len(x) == 1 else "True" if len(set(x)) == 1 else "False"),
                                                        ('mode_allowed_amount', lambda x: None if len(set(x)) == 1 else stats.mode(x).mode[0]/100),
                                                        ('mode_frequency', lambda x: None if len(set(x)) == 1 else sum(x == stats.mode(x).mode[0]))
                                                        ]
                                 }
                                 )

    pivot_table.columns = pivot_table.columns.droplevel(0)

    for service_year in range(min_service_year, max_service_year + 1, 1):
        pivot_table['claim_count', service_year].fillna(0, inplace=True)

    pivot_table['claim_count', 'total'] = 0

    for service_year in range(min_service_year, max_service_year + 1, 1):
        pivot_table['claim_count', 'total'] += pivot_table['claim_count', service_year]

    pivot_table = pivot_table.reorder_levels([1, 0], axis=1).sort_index(axis=1)

    pivot_table = pivot_table.reset_index().sort_values([('total', 'claim_count')], ascending=False).reset_index()
    pivot_table.drop(columns=['index'], inplace=True)

    for service_year in range(min_service_year, max_service_year + 1, 1):
        pivot_table.loc[pivot_table[service_year, 'claim_count'] != 0, (service_year, 'claim_count')] = '=HYPERLINK("http://mllearn.hopto.org:8000/browseclaims/1/?procedure=' + pivot_table['procedure'].astype(str) + '&npi=' + pivot_table['provider_npi'].astype(str) + '&city_state=' + pivot_table['provider_city_state'] + '&year=' + str(service_year) + '", "' + pivot_table[service_year, 'claim_count'].astype(int).astype(str) + '")'

    pivot_table.to_excel(writer, sheet_name='Useful INN Claims Pivot')

    pivot_table.columns = [str(x[0]) + ('' if x[1] == '' else '_{}'.format(x[1])) for x in pivot_table.columns.ravel()]

    pivot_table.to_excel(writer, sheet_name='Useful INN Claims Sorted', index=False)

    writer.save()

    print("Path of output file is '{path}'".format(path=name_of_output_xlsx))


def find_and_process_npis_cities(betos_type, provider_city_states, deidentified_claim_file_path, betos_file_path, betos_procedure_mapping_path):
    claims, betos_names, betos_concated = do_basic_load(deidentified_claim_file_path, betos_file_path,
                                                        betos_procedure_mapping_path)

    useful_claims = claims[(claims['is_innetwork'] == is_innetwork_when_in_network)
                           & (claims['provider_npi'] != 0)
                           & (claims['cob_amount'] == 0)
                           & (claims['modifier'].isnull())
                           & (claims['allowed_amount'] != 0)]\
        .merge(betos_concated, how="inner", on=['procedure'])

    useful_claims = useful_claims[useful_claims['betos_type'] == betos_type]
    for provider_city_state in provider_city_states:
        useful_claim_provider_city_state = useful_claims[useful_claims['provider_city_state'] == provider_city_state]
        find_and_process_npis(useful_claim_provider_city_state, betos_type, provider_city_state)


def find_and_process_npis(useful_claims, betos_type, provider_city_state):
    distinct_provider_npis = useful_claims['provider_npi'].unique()
    for provider_npi in distinct_provider_npis:
        provider_npi = int(provider_npi)
        npi_useful_claims = useful_claims[useful_claims['provider_npi'] == provider_npi]
        get_ranges(npi_useful_claims, provider_npi, betos_type, provider_city_state)


def get_ranges(useful_claims, provider_npi, betos_type, provider_city_state):
    # if provider_npi != 1376592345:
    #     return
    # Find useful claims year wise and sort the claim and prepare range
    # claims, betos_names, betos_concated = do_basic_load(deidentified_claim_file_path, betos_file_path,
    #                                                     betos_procedure_mapping_path)
    #
    # useful_claims = claims[(claims['is_innetwork'] == is_innetwork_when_in_network)
    #                        & (claims['provider_npi'] != 0)
    #                        & (claims['cob_amount'] == 0)
    #                        & (claims['modifier'].isnull())
    #                        & (claims['allowed_amount'] != 0)
    #                        & (claims['provider_npi'] == provider_npi)
    #                        & (claims['provider_city_state'] == provider_city_state)]\
    #     .merge(betos_concated, how="inner", on=['procedure'])
    #
    # useful_claims = useful_claims[(useful_claims['betos_type'] == betos_type)]
    tolerance = 0.01
    distinct_procedures = useful_claims['procedure'].unique()
    # range_array = list()
    procedure_list = list()
    required_dict = {'provider_npi': provider_npi,
                     'provider_city_state': provider_city_state,
                     'procedures': procedure_list
                     }


    # required_dict = {'procedures': range_array}
    for procedure in distinct_procedures:
        date_ranges_array = list()
        procedure_dict = {'procedure': procedure, 'date_ranges': date_ranges_array}
        procedure_list.append(procedure_dict)


        # distinct_uos = useful_claims[useful_claims['procedure'] == procedure]['uos'].unique()
        # # print(distinct_uos)
        # base_uos = min(distinct_uos) / 100
        # range_list = list()
        # range_array.append({procedure: range_list})
        claim_rows = useful_claims[(useful_claims['procedure'] == procedure)].sort_values(['service_start_date'])
        # claim_rows = useful_claims[(useful_claims['procedure'] == procedure)].sort_values(['paid_date'])
        for index, claim_row in claim_rows.iterrows():
            service_start_date = claim_row['service_start_date']
            paid_date = claim_row['paid_date']
            paid_date_year = int(paid_date.strftime('%Y'))
            service_year = int(service_start_date.strftime('%Y'))
            service_start_date_str = claim_row['service_start_date'].strftime('%Y-%m-%d')
            allowed_amount = float(claim_row['allowed_amount'] / claim_row['uos'])

            if len(date_ranges_array) == 0 or date_ranges_array[len(date_ranges_array) - 1]['year'] != str(service_year):
                date_ranges_year = list()

                year_dict = {'year': str(service_year), 'claim_count': 1, 'date_ranges': date_ranges_year, 'allowed_amounts_claim_count': {allowed_amount: 1}}
                date_ranges_year.append({
                    'start_date': service_start_date_str,
                    'end_date': service_start_date_str,
                    'paid_date_years': [paid_date_year],
                    'allowed_amount': allowed_amount,
                    'claim_count': 1})
                date_ranges_array.append(year_dict)
            else:
                date_ranges_year = date_ranges_array[len(date_ranges_array) - 1]
                date_ranges_year['claim_count'] += 1
                date_ranges_within = date_ranges_year['date_ranges']
                if len(date_ranges_within) != 0 and abs(date_ranges_within[len(date_ranges_within) - 1]['allowed_amount'] - allowed_amount) <= tolerance:
                    date_ranges_within[len(date_ranges_within) - 1]['end_date'] = service_start_date_str
                    date_ranges_within[len(date_ranges_within) - 1]['claim_count'] += 1
                    date_ranges_year['allowed_amounts_claim_count'][date_ranges_within[len(date_ranges_within) - 1]['allowed_amount']] += 1
                    if paid_date_year not in date_ranges_within[len(date_ranges_within) - 1]['paid_date_years']:
                        date_ranges_within[len(date_ranges_within) - 1]['paid_date_years'].append(paid_date_year)
                else:
                    date_ranges_within.append({
                        'start_date': service_start_date_str,
                        'end_date': service_start_date_str,
                        'paid_date_years': [paid_date_year],
                        'allowed_amount': allowed_amount,
                        'claim_count': 1
                    })

                    if allowed_amount in date_ranges_year['allowed_amounts_claim_count']:
                        date_ranges_year['allowed_amounts_claim_count'][allowed_amount] += 1
                    else:
                        date_ranges_year['allowed_amounts_claim_count'][allowed_amount] = 1


            # if len(date_ranges_array) > 0 and date_ranges_array[len(date_ranges_array) - 1]['year'] == str(service_year)
            #
            # # service_start_date = claim_row['paid_date'].strftime('%Y-%m-%d')
            # uos = claim_row['uos'] / 100
            # if uos == base_uos:
            #     allowed_amount = claim_row['allowed_amount'] / 100
            # else:
            #     allowed_amount = (claim_row['allowed_amount'] * base_uos) / (100 * uos)
            #
            # if len(range_list) !=0
            # if len(range_list) != 0 and abs(range_list[len(range_list) - 1]['allowed_amount'] - allowed_amount) <= tolerance:
            #     range_list[len(range_list) - 1]['end_date'] = service_start_date
            #     range_list[len(range_list) - 1]['claim_count'] = range_list[len(range_list) - 1]['claim_count'] + 1
            # else:
            #     range_list.append({
            #         "allowed_amount": allowed_amount,
            #         "start_date": service_start_date,
            #         "end_date": service_start_date,
            #         "uos": base_uos,
            #         "claim_count": 1
            #     })

    # import json
    # print(json.dumps(range_array, sort_keys=True, indent=4))

    # print(required_dict)
    # print('wow12345')
    required_dict_json = json.dumps(required_dict)
    # print('wow123456')
    # print(required_dict_json)

    # 1st case 1 procedure and just one one range for each year

    # print(set([len(procedure_year['date_ranges']) for procedure in required_dict['procedures'] for procedure_year in procedure['date_ranges']]))
    set_for_len_date_ranges = set([len(procedure_year['date_ranges']) for procedure in required_dict['procedures'] for procedure_year in procedure['date_ranges']])
    if max(set_for_len_date_ranges) == 1:
        process_records_with_max_one_range(provider_npi,
                                           betos_type,
                                           provider_city_state,
                                           required_dict,
                                           useful_claims)

    elif max(set_for_len_date_ranges) == 2:
        process_records_with_max_two_range(provider_npi,
                                           betos_type,
                                           provider_city_state,
                                           required_dict,
                                           useful_claims)
    else:
        process_records_with_more_than_two_ranges(provider_npi, betos_type, provider_city_state, required_dict,
                                                  useful_claims)


# Los Angeles, CA
# New York, NY
# get_ranges(1487766192, 'ov', 'New York, NY', 'C:\\Machine_Learning\\mllearn\\deident_claim_data_matched.tsv ',
#           'C:\\Machine_Learning\\mllearn\\betos_names.txt',
#           'C:\\Machine_Learning\\mllearn\\betpuf14.txt')

def process_records_with_more_than_two_ranges(provider_npi, betos_type, provider_city_state, required_dict, useful_claims):
    # Keeping it simple we are keeping annual contracts
    average_allowed_amount_more_than_2_ranges(required_dict)
    write_to_db_more_than_2_ranges(provider_npi, betos_type, provider_city_state, required_dict, useful_claims)


def process_records_with_max_two_range(provider_npi, betos_type, provider_city_state, required_dict, useful_claims):
    procedures_with_errors = list()
    year_ranges_all_procedure_with_2_ranges = list()

    count_proc_2_ranges = 0

    for procedure in required_dict['procedures']:
        procedure_name = procedure['procedure']
        original_date_ranges = procedure['date_ranges']

        years_with_2_date_ranges = [proc_year_date_ranges for proc_year_date_ranges in procedure['date_ranges'] if
                                    len(proc_year_date_ranges['date_ranges']) == 2]
        if len(years_with_2_date_ranges) > 0:
            count_proc_2_ranges += 1
            year_ranges_all_procedure_with_2_ranges.extend(years_with_2_date_ranges)

        if len(years_with_2_date_ranges) > 1:
            try:
                merge_date_ranges_same_proc_different_year_all_2_ranges(years_with_2_date_ranges, True)
            except:
                procedures_with_errors.append(procedure_name)

    combined_lower_range, combined_higher_range = None, None

    is_error = False
    if len(procedures_with_errors) > 0:
        is_error = True

    # Now we have new ranges, now try to find merge of date across procedures for the dates with two ranges
    # If we pick one year of multi-range from each procedure which has multi-range we will arrive
    if not is_error and count_proc_2_ranges >= 1:  # This mean atleast two years have multi ranges
        try:
            combined_lower_range, combined_higher_range = merge_date_ranges_same_proc_different_year_all_2_ranges(
                year_ranges_all_procedure_with_2_ranges,
                True)
        except:
            is_error = True
            # Merging error across procedures
            # Make entries in db

    if is_error:
        # Make entries in db
        message = "Max two ranges case.Not able to merge ranges within procedure(s)"
        cur.execute("""insert into public.npi_city_comments (betos_type, npi, city, ranges, comments) values 
                                            (%s, %s, %s, %s, %s)
                                        """,
                    (betos_type, provider_npi, provider_city_state, json.dumps(required_dict), message))
        conn.commit()
        return

    # in case we have all 1 range entry either belonging to 1st part or belonging to 2nd part we are fine
    # Prepare a list of all records which belong to both ranges
    # Prepare a list of all records which belong to 1st and mid
    # Prepare a list which belong to mid and 2nd
    # print(required_dict)

    years_with_1_date_range = [proc_year_date_ranges for procedure in required_dict['procedures']
                               for proc_year_date_ranges in procedure['date_ranges']
                               if len(proc_year_date_ranges['date_ranges']) == 1]

    for year_with_1_date_range in years_with_1_date_range:
        set_new_range_for_1_year_range(year_with_1_date_range, combined_lower_range, combined_higher_range)

    # There will be few records remaining
    # See now the records which still has one range. It mean it hasn't been processed it
    get_new_range_using_one_date_range_with_provided_range(required_dict, combined_lower_range, combined_higher_range)

    # Now extend this range to cover year
    extend_range_to_cover_year(combined_lower_range, combined_higher_range)

    set_new_range(required_dict, combined_lower_range, combined_higher_range, useful_claims)

    # reassign allowed_amount by taking averages
    reassign_allowed_amount_max_two_year_json(required_dict)
    write_to_db_max_2_ranges(combined_lower_range, combined_higher_range, provider_npi, betos_type, provider_city_state, required_dict, useful_claims)
    # print(required_dict)


def average_allowed_amount_more_than_2_ranges(required_dict):
    for procedure in required_dict['procedures']:
        for proc_year_date_ranges in procedure['date_ranges']:
            year = proc_year_date_ranges['year']
            if len(proc_year_date_ranges['date_ranges']) > 1:
                ccount_aamount_lst = [(x['claim_count'], x['allowed_amount']) for x in proc_year_date_ranges['date_ranges']]

                average_allowed_amount = round(sum(map(lambda x: x[0] * x[1], ccount_aamount_lst))/
                                               sum([x[0] for x in ccount_aamount_lst]), 2)

                proc_year_date_ranges['average_allowed_amount'] = average_allowed_amount


def write_to_db_more_than_2_ranges(provider_npi, betos_type, provider_city_state, required_dict, useful_claims):
    distinct_procedures = useful_claims['procedure'].unique()
    is_averaged_allowed_amount_present = False
    total_procedure_count = len(distinct_procedures)

    actual_range = {'start': '01-01', 'end': '12-31'}

    for procedure in required_dict['procedures']:
        procedure_name = procedure['procedure']
        for proc_year_date_ranges in procedure['date_ranges']:
            year = proc_year_date_ranges['year']
            claim_count = proc_year_date_ranges['claim_count']
            message = "Average allowed amount used b/c more than two date ranges.Annual contract."
            if claim_count == 1:
                message += "One Claim."

            cur.execute("""
                                                insert into public.procedure_npi_city_year_comments (betos_type, npi, procedure, city, year, ranges, comments) values
                                                (%s, %s, %s, %s, %s, %s, %s)
                                                """,
                        (betos_type, provider_npi, procedure_name, provider_city_state, year,
                         json.dumps(proc_year_date_ranges),
                         message))

        message = "Average allowed amount used b/c more than two date ranges.Annual contract."

        if total_procedure_count == 1:
            message += 'Only Procedure for NPI, Betos Type and City'

        contract_price = prepare_contract_price_json(procedure, actual_range)
        # print(contract_price)
        cur.execute("""
                                insert into public.procedure_npi_city_comments (betos_type, npi, procedure, city, ranges, comments, contract_price_json) values
                                (%s, %s, %s, %s, %s, %s, %s)
                                """,
                    (betos_type, provider_npi, procedure_name, provider_city_state, json.dumps(procedure),
                     message, json.dumps(contract_price)))

    message = 'Average allowed amount used b/c more than two date ranges.Annual contract.'

    if total_procedure_count == 1:
        message += 'Only One Procedure for NPI,Betos Type and City.'

    cur.execute("""insert into public.npi_city_comments (betos_type, npi, city, ranges, comments, contract_period) values
                                            (%s, %s, %s, %s, %s, %s)
                                        """,
                (betos_type, provider_npi, provider_city_state, json.dumps(required_dict), message, json.dumps(actual_range)))
    conn.commit()


def prepare_contract_price_json(procedure, contract_period):
    base_list = list()
    for date_range in procedure['date_ranges']:
        year = date_range['year']
        for year_date_range in date_range['date_ranges']:
            start_date = year_date_range['start_date']
            claim_count = year_date_range['claim_count']
            allowed_amount = year_date_range['allowed_amount'] if 'allowed_amount' in year_date_range else None
            contract_year = get_contract_year_annual_contract(contract_period, start_date)
            if len(base_list) != 0 and base_list[len(base_list) - 1]['year'] == contract_year:
                required_entry = base_list[len(base_list) - 1]
                required_entry['claim_count'] += claim_count
                if allowed_amount is not None:
                    required_entry['allowed_amount'] = allowed_amount
            else:
                if claim_count != 0:
                    required_entry = dict()
                    required_entry['year'] = contract_year
                    required_entry['claim_count'] = claim_count
                    if allowed_amount is not None:
                        required_entry['allowed_amount'] = allowed_amount
                    base_list.append(required_entry)

        if 'average_allowed_amount' in date_range:
            base_list[len(base_list) - 1]['allowed_amount'] = date_range['average_allowed_amount']

    return base_list


def write_to_db_max_2_ranges(lower_range, higher_range, provider_npi, betos_type, provider_city_state, required_dict, useful_claims):
    distinct_procedures = useful_claims['procedure'].unique()
    is_averaged_allowed_amount_present = False
    total_procedure_count = len(distinct_procedures)

    actual_range = {'start': higher_range['start_string'], 'end': lower_range['end_string']}

    for procedure in required_dict['procedures']:
        procedure_name = procedure['procedure']
        is_averaged_allowed_amount_proc_present = False
        for proc_year_date_ranges in procedure['date_ranges']:
            year = proc_year_date_ranges['year']
            message = ''
            if len([date_range['actual_allowed_amount'] for date_range in proc_year_date_ranges['date_ranges'] if 'actual_allowed_amount' in date_range]) != 0:
                is_averaged_allowed_amount_present = True
                is_averaged_allowed_amount_proc_present = True

                message += 'Average allowed amount used.'

            message += "Two Date Ranges per year.Mid-Year Contract."

            cur.execute("""
                                            insert into public.procedure_npi_city_year_comments (betos_type, npi, procedure, city, year, ranges, comments) values 
                                            (%s, %s, %s, %s, %s, %s, %s)
                                            """,
                        (betos_type, provider_npi, procedure_name, provider_city_state, year,
                         json.dumps(proc_year_date_ranges),
                         message))

        message = ''
        if is_averaged_allowed_amount_proc_present:
            message += 'Average allowed amount used.'
        message += 'Two Date Ranges per year.Mid-Year Contract.'

        if total_procedure_count == 1:
            message += 'Only Procedure for NPI, Betos Type and City'

        contract_price = prepare_contract_price_json(procedure, actual_range)
        # print(contract_price)

        cur.execute("""
                            insert into public.procedure_npi_city_comments (betos_type, npi, procedure, city, ranges, comments, contract_price_json) values 
                            (%s, %s, %s, %s, %s, %s, %s)
                            """, (betos_type, provider_npi, procedure_name, provider_city_state, json.dumps(procedure),
                                  message, json.dumps(contract_price)))

    message = ''
    if is_averaged_allowed_amount_present:
        message += 'Average allowed amount used.'
    message += 'Two Date Ranges per year.Mid-Year Contract.'

    if total_procedure_count == 1:
        message += 'Only One Procedure for NPI,Betos Type and City.'

    cur.execute("""insert into public.npi_city_comments (betos_type, npi, city, ranges, comments, contract_period) values 
                                        (%s, %s, %s, %s, %s, %s)
                                    """,
                (betos_type, provider_npi, provider_city_state, json.dumps(required_dict), message, json.dumps(actual_range)))
    conn.commit()


def reassign_allowed_amount_max_two_year_json(required_dict):
    tolerance = 0.01

    for procedure in required_dict['procedures']:
        for year_range in procedure['date_ranges']:
            service_year = year_range['year']
            next_service_year = str(int(service_year) + 1)
            next_service_year_range_list = [date_range for date_range in procedure['date_ranges'] if
                                            date_range['year'] == next_service_year]
            if len(next_service_year_range_list) == 0:
                continue

            next_service_year_range = next_service_year_range_list[0]

            if year_range['date_ranges'][1]['claim_count'] == 0 or next_service_year_range[
                'date_ranges'][0]['claim_count'] == 0 or abs(
                year_range['date_ranges'][1]['allowed_amount'] - next_service_year_range['date_ranges'][0]['allowed_amount']) <= tolerance:

                continue
            else:
                claim_count_1st_part = year_range['date_ranges'][1]['claim_count']
                claim_count_2nd_part = next_service_year_range['date_ranges'][0]['claim_count']
                allowed_amount_1st_part = year_range['date_ranges'][1]['allowed_amount']
                allowed_amount_2nd_part = next_service_year_range['date_ranges'][0]['allowed_amount']

                avg_amount = round((allowed_amount_1st_part * claim_count_1st_part +
                                    allowed_amount_2nd_part * claim_count_2nd_part) / (claim_count_1st_part + claim_count_2nd_part), 2)

                year_range['date_ranges'][1]['actual_allowed_amount'] = allowed_amount_1st_part
                next_service_year_range['date_ranges'][0]['actual_allowed_amount'] = allowed_amount_2nd_part
                year_range['date_ranges'][1]['allowed_amount'] = next_service_year_range['date_ranges'][0]['allowed_amount'] = avg_amount


def extend_range_to_cover_year(lower_range, higher_range):
    # Now we will extend the ranges to cover the year
    # If end of 1st part lies in one month and 2nd part atleast in next month, use the end of this month as end date and next month start date as next date
    month_part_lower_range_end = int(lower_range['end_string'].split('-')[0])
    month_part_higher_range_start = int(higher_range['start_string'].split('-')[0])
    # date_part_lower_range_end = int(combined_lower_range['end_string'].split('-')[1])
    date_part_higher_range_start = int(higher_range['start_string'].split('-')[1])
    if month_part_higher_range_start - month_part_lower_range_end > 0:
        if month_part_lower_range_end == 2:
            lower_range['end_string'] = '02-29'
            higher_range['start_string'] = '03-01'
        else:
            lower_range['end_string'] = str(month_part_lower_range_end).zfill(2) + '-' + str(
                monthrange(2016, month_part_lower_range_end)[1])
            higher_range['start_string'] = str(month_part_lower_range_end + 1).zfill(2) + '-01'
    else:
        lower_range['end_string'] = str(month_part_lower_range_end).zfill(2) + '-' + str(
            date_part_higher_range_start - 1).zfill(2)


def get_new_range_using_one_date_range_with_provided_range(required_dict, lower_range, higher_range):
    years_remaining_with_one_range = [proc_year_date_ranges for procedure in required_dict['procedures']
                                      for proc_year_date_ranges in procedure['date_ranges']
                                      if len(proc_year_date_ranges['date_ranges']) == 1]

    counter = 0

    if len(years_remaining_with_one_range) > 0:
        # Lets loop through it and look through new range:
        for year_remaining_with_one_range in years_remaining_with_one_range:
            service_start_date = year_remaining_with_one_range['date_ranges'][0]['start_date']
            service_end_date = year_remaining_with_one_range['date_ranges'][0]['end_date']
            service_start_date_date_part = '-'.join(service_start_date.split('-')[1:])
            service_end_date_date_part = '-'.join(service_end_date.split('-')[1:])
            if lower_range['start_string'] <= service_start_date_date_part <= lower_range['end_string'] < service_end_date_date_part < higher_range['start_string']:
                lower_range['end_string'] = service_end_date_date_part
                del years_remaining_with_one_range[counter]
                counter -= 1
            counter += 1

    counter = 0
    if len(years_remaining_with_one_range) > 0:
        # Lets loop through it and look through new range:
        for year_remaining_with_one_range in years_remaining_with_one_range:
            service_start_date = year_remaining_with_one_range['date_ranges'][0]['start_date']
            service_end_date = year_remaining_with_one_range['date_ranges'][0]['end_date']
            service_start_date_date_part = '-'.join(service_start_date.split('-')[1:])
            service_end_date_date_part = '-'.join(service_end_date.split('-')[1:])
            if lower_range['end_string'] < service_start_date_date_part < higher_range['start_string'] <= service_end_date_date_part <= higher_range['end_string']:
                higher_range['start_string'] = service_start_date_date_part
                del years_remaining_with_one_range[counter]
                counter -= 1
            counter += 1


def process_records_with_max_one_range(provider_npi, betos_type, provider_city_state, required_dict, useful_claims):

    distinct_procedures = useful_claims['procedure'].unique()
    total_procedure_count = len(distinct_procedures)

    actual_range = {'start': '01-01', 'end': '12-31'}

    for procedure in required_dict['procedures']:
        procedure_name = procedure['procedure']
        for proc_year_date_ranges in procedure['date_ranges']:
            year = proc_year_date_ranges['year']
            date_range = proc_year_date_ranges['date_ranges'][0]
            # print(json.dumps(date_range))
            claim_count = date_range['claim_count']
            message = "One Date Range per year.Annual contract."
            if claim_count == 1:
                message += "One Claim."
            cur.execute("""
                                        insert into public.procedure_npi_city_year_comments (betos_type, npi, procedure, city, year, ranges, comments) values 
                                        (%s, %s, %s, %s, %s, %s, %s)
                                        """,
                        (betos_type, provider_npi, procedure_name, provider_city_state, year, json.dumps(proc_year_date_ranges),
                         message))

        message = 'One Date Range per year.Annual contract.'
        if total_procedure_count == 1:
            message += 'Only Procedure for NPI, Betos Type and City'

        contract_price = prepare_contract_price_json(procedure, actual_range)
        # print(contract_price)

        cur.execute("""
                        insert into public.procedure_npi_city_comments (betos_type, npi, procedure, city, ranges, comments, contract_price_json) values 
                        (%s, %s, %s, %s, %s, %s, %s)
                        """, (betos_type, provider_npi, procedure_name, provider_city_state, json.dumps(procedure),
                              message, json.dumps(contract_price)))

    message = 'One Date Range per year.Annual contract.'
    if total_procedure_count == 1:
        message += 'Only One Procedure for NPI,Betos Type and City.'

    cur.execute("""insert into public.npi_city_comments (betos_type, npi, city, ranges, comments, contract_period) values 
                                    (%s, %s, %s, %s, %s, %s)
                                """,
                (betos_type, provider_npi, provider_city_state, json.dumps(required_dict), message, json.dumps(actual_range)))
    conn.commit()


def set_new_range(input_json, lower_range, higher_range, useful_claims=None):
    all_date_ranges = [(procedure['procedure'], date_range) for procedure in input_json['procedures'] for date_range in procedure['date_ranges']]
    for procedure_name, year_range in all_date_ranges:
        # if procedure_name == "99213":
        #     print(procedure_name)
        len_year_range = len(year_range['date_ranges'])
        if len_year_range == 2:
            set_new_range_for_2_year_range(year_range, lower_range, higher_range)
        elif len_year_range == 1:
            set_new_range_for_1_year_range(year_range, lower_range, higher_range, procedure_name, useful_claims)

    # print(json.dumps(input_json))


def set_new_range_for_1_year_range(year_range, lower_range, higher_range, procedure_name=None, useful_claims=None):
    if len(year_range['date_ranges']) == 1:
        year = year_range['year']
        start_date = year_range['date_ranges'][0]['start_date']
        end_date = year_range['date_ranges'][0]['end_date']
        allowed_amount = year_range['date_ranges'][0]['allowed_amount']
        if lower_range['start_string'] <= '-'.join(start_date.split('-')[1:]) \
                and '-'.join(end_date.split('-')[1:]) <= lower_range['end_string']:

            year_range['date_ranges'][0]['start_date'] = get_date_in_correct_format_29th_Feb(
                str(year) + '-' + lower_range['start_string'], year, True)
            year_range['date_ranges'][0]['end_date'] = get_date_in_correct_format_29th_Feb(
                str(year) + '-' + lower_range['end_string'], year, False)

            next_range = {
                'start_date': get_date_in_correct_format_29th_Feb(
                    str(year) + '-' + higher_range['start_string'], year, True),
                'end_date': get_date_in_correct_format_29th_Feb(
                    str(year) + '-' + higher_range['end_string'], year, False),
                'claim_count': 0
            }

            year_range['date_ranges'].append(next_range)

        elif higher_range['start_string'] <= '-'.join(start_date.split('-')[1:]) \
                and '-'.join(end_date.split('-')[1:]) <= higher_range['end_string']:

            year_range['date_ranges'][0]['start_date'] = get_date_in_correct_format_29th_Feb(
                str(year) + '-' + higher_range['start_string'], year, True)
            year_range['date_ranges'][0]['end_date'] = get_date_in_correct_format_29th_Feb(
                str(year) + '-' + higher_range['end_string'], year, False)

            previous_range = {
                'start_date': get_date_in_correct_format_29th_Feb(
                    str(year) + '-' + lower_range['start_string'], year, True),
                'end_date': get_date_in_correct_format_29th_Feb(
                    str(year) + '-' + lower_range['end_string'], year, False),
                'claim_count': 0
            }

            year_range['date_ranges'].insert(0, previous_range)
        elif useful_claims is not None and lower_range['start_string'] <= '-'.join(start_date.split('-')[1:]) <= lower_range['end_string'] and higher_range['start_string'] <= '-'.join(end_date.split('-')[1:]) <= higher_range['end_string']:
            # Here we will need to reconstruct json again
            # Find number of claims
            # 1st find number of records in lower range
            claim_count_1st_half = 0
            paid_years_1st_half = set()
            claim_count_2nd_half = 0
            paid_years_2nd_half = set()

            year_range['date_ranges'][0]['start_date'] = get_date_in_correct_format_29th_Feb(str(year) + '-' + lower_range['start_string'], year, True)
            year_range['date_ranges'][0]['end_date'] = get_date_in_correct_format_29th_Feb(str(year) + '-' + lower_range['end_string'], year, False)

            upper_bound_json = dict()
            upper_bound_json['start_date'] = get_date_in_correct_format_29th_Feb(str(year) + '-' + higher_range['start_string'], year, True)
            upper_bound_json['end_date'] = get_date_in_correct_format_29th_Feb(str(year) + '-' + higher_range['end_string'], year, False)

            year_range['date_ranges'].append(upper_bound_json)

            claims_in_service_year = useful_claims[(useful_claims['procedure'] == procedure_name) & (useful_claims['service_year'] == int(year))]
            for index, claim_row in claims_in_service_year.iterrows():
                service_start_date = claim_row['service_start_date']
                paid_date = claim_row['paid_date']
                paid_date_year = int(paid_date.strftime('%Y'))
                service_start_date_str = claim_row['service_start_date'].strftime('%Y-%m-%d')
                allowed_amount = float(claim_row['allowed_amount'] / claim_row['uos'])

                if year_range['date_ranges'][0]['start_date'] <= service_start_date_str <= year_range['date_ranges'][0]['end_date']:
                    claim_count_1st_half += 1
                    paid_years_1st_half.add(paid_date_year)

                else:
                    claim_count_2nd_half += 1
                    paid_years_2nd_half.add(paid_date_year)

            year_range['date_ranges'][0]['claim_count'] = claim_count_1st_half
            year_range['date_ranges'][1]['claim_count'] = claim_count_2nd_half

            if claim_count_1st_half == 0:
                del year_range['date_ranges'][0]['allowed_amount']
                del year_range['date_ranges'][0]['paid_date_years']
            else:
                year_range['date_ranges'][0]['paid_date_years'] = sorted(list(paid_years_1st_half))

            if claim_count_2nd_half > 0:
                year_range['date_ranges'][1]['allowed_amount'] = allowed_amount

                year_range['date_ranges'][1]['paid_date_years'] = sorted(list(paid_years_2nd_half))


def set_new_range_for_2_year_range(year_range, lower_range, higher_range):
    if len(year_range['date_ranges']) == 2:
        service_year = str(year_range['year'])
        year_range['date_ranges'][0]['start_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' + lower_range['start_string'], service_year, True)
        year_range['date_ranges'][0]['end_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' + lower_range['end_string'], service_year, False)
        year_range['date_ranges'][1]['start_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' + higher_range['start_string'], service_year, True)
        year_range['date_ranges'][1]['end_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' + higher_range['end_string'], service_year, False)


def merge_ranges(single_date_range_across_years):
    required_range = {'start_string': '-'.join(single_date_range_across_years[0]['start_date'].split('-')[1:]),
                      'end_string': '-'.join(single_date_range_across_years[0]['end_date'].split('-')[1:])}

    if len(single_date_range_across_years) > 1:
        counter = 1

        while counter < len(single_date_range_across_years):
            start_string = required_range['start_string']
            str_temp = '-'.join(single_date_range_across_years[counter]['start_date'].split('-')[1:])
            if str_temp < start_string:
                start_string = str_temp

            end_string = required_range['end_string']
            str_temp = '-'.join(single_date_range_across_years[counter]['end_date'].split('-')[1:])
            if str_temp > end_string:
                end_string = str_temp

                required_range = {'start_string': start_string, 'end_string': end_string}

            counter += 1

    return required_range


def get_ranges_using_years_and_2_ranges_without_year(years, combined_lower_range, combined_higher_range)  :
    new_date_range_list = list()
    for year in years:
        year_json = {
            'year': year,
            'date_ranges': [
                {'start_date': str(year) + '-' + combined_lower_range['start_string'],
                 'end_date': str(year) + '-' + combined_lower_range['end_string']},
                {'start_date': str(year) + '-' + combined_higher_range['start_string'],
                 'end_date': str(year) + '-' + combined_higher_range['end_string']}
            ]
        }
        new_date_range_list.append(year_json)
    return new_date_range_list


def merge_date_ranges_same_proc_different_year_all_2_ranges(years_with_2_date_ranges, try_extending= False):
    if len(years_with_2_date_ranges) > 0:
        # Lets 1st merge lower range
        lower_ranges = [x['date_ranges'][0] for x in years_with_2_date_ranges]
        combined_lower_range = merge_ranges(lower_ranges)

        # Now merge higher range
        higher_ranges = [x['date_ranges'][1] for x in years_with_2_date_ranges]
        combined_higher_range = merge_ranges(higher_ranges)

        # Lets verify there is no overlap between combined lower range and combined higher range
        if combined_lower_range['start_string'] <= combined_higher_range['start_string'] <= combined_lower_range['end_string'] or combined_higher_range['start_string'] <= combined_lower_range['start_string'] <= combined_higher_range['end_string']:
            # Here overlap between two ranges:
            raise ValueError('Overlap of joined ranges')

        if try_extending:
            combined_lower_range['start_string'] = '01-01'
            combined_higher_range['end_string'] = '12-31'

        # print(combined_lower_range)
        # print(combined_higher_range)
        # Now construct the newer range for each year
        # Loop through this year list and prepare date ranges for each other
        # all_years_with_two_ranges = [x['year'] for x in years_with_2_date_ranges]
        # new_date_range_list = get_ranges_using_years_and_2_ranges_without_year(all_years_with_two_ranges, combined_lower_range, combined_higher_range)
        #
        # # Now add allowed_amount to these intervals verify this new_date_range_list against all_years_with_two_ranges so verify that all ranges can be explained
        # counter = 0

        for year_with_2_date_range in years_with_2_date_ranges:
            set_new_range_for_2_year_range(year_with_2_date_range, combined_lower_range, combined_higher_range)
            # service_year = str(year_with_2_date_range['year'])
            # year_with_2_date_range['date_ranges'][0]['start_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' +  combined_lower_range['start_string'], service_year, True)
            # year_with_2_date_range['date_ranges'][0]['end_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' +  combined_lower_range['end_string'], service_year, False)
            # year_with_2_date_range['date_ranges'][1]['start_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' +  combined_higher_range['start_string'], service_year, True)
            # year_with_2_date_range['date_ranges'][1]['end_date'] = get_date_in_correct_format_29th_Feb(service_year + '-' +  combined_higher_range['end_string'], service_year, False)

            # print(year_with_2_date_range)
        return combined_lower_range, combined_higher_range
    # elif len(years_with_2_date_ranges) == 1:
    #     combined_lower_range = {'start_string': '-'.join(years_with_2_date_ranges[0]['date_ranges'][0]['start_date'].split('-')[1:]),
    #                     'end_string': '-'.join(years_with_2_date_ranges[0]['date_ranges'][0]['end_date'].split('-')[1:])}
    #
    #     combined_higher_range = {'start_string': '-'.join(years_with_2_date_ranges[0]['date_ranges'][1]['start_date'].split('-')[1:]),
    #                     'end_string': '-'.join(years_with_2_date_ranges[0]['date_ranges'][1]['end_date'].split('-')[1:])}
    #
    #     if try_extending:
    #         combined_lower_range['start_string'] = '01-01'
    #         combined_higher_range['end_string'] = '12-31'
    #
    #
    #     return combined_lower_range, combined_higher_range
    else:
        return None, None


def get_date_in_correct_format_29th_Feb(date_str, year, is_start_date):
    return_str = date_str
    if is_start_date:
        # Here we will make date as 1st March
        try:
            datetime.strptime(date_str, "%Y-%m-%d").date()
        except:
            return_str = str(year) + '-03-01'
    else:
        # Here it is end date, make date as 28th Feb
        try:
            datetime.strptime(date_str, "%Y-%m-%d").date()
        except:
            return_str = str(year) + '-02-28'
    return return_str

find_and_process_npis_cities('ov',
                             ['Los Angeles, CA', 'New York, NY', 'Indianapolis, IN'],
                             'C:\\Machine_Learning\\mllearn\\deident_claim_data_matched.tsv',
                             'C:\\Machine_Learning\\mllearn\\betos_names.txt',
                             'C:\\Machine_Learning\\mllearn\\betpuf14.txt')
# get_stats('C:\\Machine_Learning\\mllearn\\deident_claim_data_matched.tsv ',
#           'C:\\Machine_Learning\\mllearn\\betos_names.txt',
#           'C:\\Machine_Learning\\mllearn\\betpuf14.txt')