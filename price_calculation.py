import psycopg2
from generic_functions import get_contract_year_annual_contract
import json
import sklearn
from sklearn.linear_model import LinearRegression
from numpy import array

conn = psycopg2.connect(dbname="mllearn", user="postgres", password="aditya12#", host="localhost")
cur = conn.cursor()


def get_betos_type(procedure):
    result = None
    cur.execute("select betos_type from public.betos_procedure where procedure= %s", (procedure,))
    result_temp = cur.fetchall()
    if result_temp:
        result = result_temp[0][0]

    return result


def get_contract_for_npi_city_betos_type(npi, city_state, betos_type):
    result = None
    cur.execute("""
        select contract_period from public.npi_city_comments where npi= %s and betos_type=%s and city=%s
        """, (npi, betos_type, city_state))

    result_temp = cur.fetchall()
    if result_temp:
        result = result_temp[0][0]
        if result is not None:
            result = json.loads(result)

    return result

def get_contract_price_for_npi_city_procedure_betos_type(npi, city_state, procedure, betos_type):
    result = None
    cur.execute("""
        select contract_price_json from public.procedure_npi_city_comments 
            where npi= %s 
            and betos_type=%s 
            and city=%s
            and procedure=%s
        """, (npi, betos_type, city_state, procedure))

    result_temp = cur.fetchall()
    if result_temp:
        result = result_temp[0][0]
        if result is not None:
            result = json.loads(result)

    return result


def get_cost(npi, city_state, procedure, date):
    # Fetch betos type

    betos_type = get_betos_type(procedure)
    if betos_type is None:
        print('Procedure not found')
        return
    elif betos_type not in ['ov', 'imaging']:
        print('Procedure not of ov/imaging type')
        return

    # print(betos_type)

    #Lets fetch the contract period
    contract_period = get_contract_for_npi_city_betos_type(npi, city_state, betos_type)
    if contract_period is None:
        print('Contract Range not found.')
        return

    # print(contract_period)
    contract_year = get_contract_year_annual_contract(contract_period, date)
    # print(contract_year)

    contract_price = get_contract_price_for_npi_city_procedure_betos_type(npi, city_state, procedure, betos_type)
    if contract_price is None:
        print('Contract Price Json not found.')
        return

    # print(contract_price)
    # Now lets predict the price

    # If contract_year has an entry in contract_price, then return that price
    year_price_list = [year_contract_price['allowed_amount'] for year_contract_price in contract_price if year_contract_price['year'] == contract_year]
    if year_price_list:
        return year_price_list[0]

    # If we have two year entries for contract_price and both has same price, then return that price
    # This is fine for coming year
    # But what happens in case its a past year. What to be done then.

    # Commenting this as suggested by Paddu
    # year_price_list = [year_contract_price for year_contract_price in contract_price if year_contract_price['year'] < contract_year]
    # if len(year_price_list) >= 2 and year_price_list[len(year_price_list) - 1]['allowed_amount'] == year_price_list[len(year_price_list) - 2]['allowed_amount']:
    #     return year_price_list[len(year_price_list) - 1]['allowed_amount']

    if len(contract_price) >= 2:
        # use linear regression to find the price
        lm = LinearRegression()
        # k = array([year_contract_price['year'] for year_contract_price in contract_price])
        # print(k.reshape(-1, 1))
        lm.fit(array([year_contract_price['year'] for year_contract_price in contract_price]).reshape(-1, 1),
               [year_contract_price['allowed_amount'] for year_contract_price in contract_price])
        return round(lm.predict(contract_year)[0], 2)

    # What about the case where year count is just 1. We can assume the price to be same.
    if len(contract_price) == 1:
        return contract_price[0]['allowed_amount']
    # Now I have to look through the records to find contract period
    # Then verify within which year provided date can be put
    # If more than one points use linear regression to plot a line
    # Then find the cost using that linera regression





print(get_cost(1043233760, 'Los Angeles, CA', '99215', '2012-10-20'))

